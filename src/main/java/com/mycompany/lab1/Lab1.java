/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author pasinee
 */
import java.util.Scanner;

/// Pasinee Chaweenet 65160077 SoftDev Sec1 ///

public class Lab1 {
    public static Scanner kb = new Scanner (System.in);
    public static String [][]tt = new String [3][3];
    public static String playerX = "X";
    public static String playerO = "O";

    public static void emptyBoard() {
        for(int i=0; i<3;i++){
            for(int j=0; j<3; j++){
                tt[i][j] = " _ ";
            }
        }

        for(int i=0; i<3;i++){
            System.out.print("|");
            for(int j=0; j<3; j++){
                System.out.print(tt[i][j]);
                System.out.print("|");
            }
            System.out.println();
        }
    }

    

    public static void playerXTurn(){
        while(true){
            System.out.println("---------------");
            System.out.println("Player X's Turn");
            
            System.out.print("Please input the row (1-3) : ");
            int row = kb.nextInt();

            System.out.print("Please input the column (1-3) : ");
            int column = kb.nextInt();

            if(tt[row-1][column-1].equals(" _ ")){
                tt[row-1][column-1] = " " + playerX + " ";

            }else{
                System.out.println("You cannot play that box");
                continue;
            }

            for(int i = 0; i<3; i++){
                System.out.print("|");
                for(int j = 0; j<3; j++){
                    System.out.print(tt[i][j]);
                    System.out.print("|");
                }
                System.out.println();
            }
            break;
        }
    }

    public static void playerOTurn(){
        while(true){
            System.out.println("---------------");
            System.out.println("Player O's Turn");
            
            System.out.print("Please input the row (1-3) : ");
            int row = kb.nextInt();

            System.out.print("Please input the column (1-3) : ");
            int column = kb.nextInt();

            if(tt[row-1][column-1].equals(" _ ")){

                tt[row-1][column-1] = " " + playerO + " ";

            }else{
                System.out.println("You cannot play that box");
                continue;
            }

            for(int i = 0; i<3; i++){
                System.out.print("|");
                for(int j = 0; j<3; j++){
                    System.out.print(tt[i][j]);
                    System.out.print("|");
                }
                System.out.println();
            }
            break;
        }
    }

    //-------------------------------------------------------------------------------------------------------------////
    public static void main(String[] args) {

        while(true){
            System.out.println("Welcome to OX !!");
            emptyBoard();

                while(true){
                    ///-------playerX----------///
                    if( !(tt[0][0].equals(" _ ")) && !(tt[0][1].equals(" _ ")) && !(tt[0][2].equals(" _ ")) 
                        && !(tt[1][0].equals(" _ ")) && !(tt[1][1].equals(" _ ")) && !(tt[1][2].equals(" _ ")) 
                        && !(tt[2][0].equals(" _ ")) && !(tt[2][1].equals(" _ ")) && !(tt[2][2].equals(" _ "))){

                        System.out.println("Draw!!!");
                        break;
                    }

                    playerXTurn();

                    if((tt[0][0].equals(" "+ playerX +" ")) && (tt[0][1].equals(" "+ playerX +" ")) && (tt[0][2].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;

                    }else if((tt[1][0].equals(" "+ playerX +" ")) && (tt[1][1].equals(" "+ playerX +" ")) && (tt[1][2].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;

                    }else if((tt[2][0].equals(" "+ playerX +" ")) && (tt[2][1].equals(" "+ playerX +" ")) && (tt[2][2].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;

                    }else if((tt[0][0].equals(" "+ playerX +" ")) && (tt[1][0].equals(" "+ playerX +" ")) && (tt[2][0].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;
                    
                    }else if((tt[0][1].equals(" "+ playerX +" ")) && (tt[1][1].equals(" "+ playerX +" ")) && (tt[2][1].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;


                    }else if((tt[0][2].equals(" "+ playerX +" ")) && (tt[1][2].equals(" "+ playerX +" ")) && (tt[2][2].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;

                    }else if((tt[0][0].equals(" "+ playerX +" ")) && (tt[1][1].equals(" "+ playerX +" ")) && (tt[2][2].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;

                    }else if((tt[0][2].equals(" "+ playerX +" ")) && (tt[1][1].equals(" "+ playerX +" ")) && (tt[2][0].equals(" "+ playerX +" "))){
                        System.out.println(playerX + " won!!!");
                        break;
                    }

                    
                    ///-------playerO----------///

                    if( !(tt[0][0].equals(" _ ")) && !(tt[0][1].equals(" _ ")) && !(tt[0][2].equals(" _ ")) 
                        && !(tt[1][0].equals(" _ ")) && !(tt[1][1].equals(" _ ")) && !(tt[1][2].equals(" _ ")) 
                        && !(tt[2][0].equals(" _ ")) && !(tt[2][1].equals(" _ ")) && !(tt[2][2].equals(" _ "))){

                        System.out.println("Draw!!!");
                        break;
                    }

                    playerOTurn();

                    if((tt[0][0].equals(" "+ playerO +" ")) && (tt[0][1].equals(" "+ playerO +" ")) && (tt[0][2].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;

                    }else if((tt[1][0].equals(" "+ playerO +" ")) && (tt[1][1].equals(" "+ playerO +" ")) && (tt[1][2].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;

                    }else if((tt[2][0].equals(" "+ playerO +" ")) && (tt[2][1].equals(" "+ playerO +" ")) && (tt[2][2].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;

                    }else if((tt[0][0].equals(" "+ playerO +" ")) && (tt[1][0].equals(" "+ playerO +" ")) && (tt[2][0].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;
                    
                    }else if((tt[0][1].equals(" "+ playerO +" ")) && (tt[1][1].equals(" "+ playerO +" ")) && (tt[2][1].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;


                    }else if((tt[0][2].equals(" "+ playerO +" ")) && (tt[1][2].equals(" "+ playerO +" ")) && (tt[2][2].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;

                    }else if((tt[0][0].equals(" "+ playerO +" ")) && (tt[1][1].equals(" "+ playerO +" ")) && (tt[2][2].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;

                    }else if((tt[0][2].equals(" "+ playerO +" ")) && (tt[1][1].equals(" "+ playerO +" ")) && (tt[2][0].equals(" "+ playerO +" "))){
                        System.out.println(playerO + " won!!!");
                        break;
                    }
                
                    
                }

                System.out.print("Would you like to play again ? (y/n) : ");
                String answer = kb.next();

                if (answer.equals("n")){
                    System.out.println();
                    System.out.println("Thanks for playing ! :-) ");
                    break;

                }else if (answer.equals("y")){
                    System.out.println();                
                    continue;
 
            }else{
                System.out.println("Invalid Character");
                continue;
            }

        }  
    }
    
}

